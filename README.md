# MovieDatabase
Silver and Alex


## Assumptions:
1. Each movie/song belongs to one genre only.
2. Each song can have only one artist.
3. If the movie/song is purchased offline (e.g. in HMV), there is no URL (a null is placed there).
4. There will not be 2 songs with same title and same album.
5. There will not be 2 movies with same title and same release year.
6. Every media item will have one unique mediaID.
7. All the data inserted is gathered from one person i.e. this is one person's media library



### 1. Give me all of the purchase information for all music that's been bought

```sql
select * from PurchaseInfo
where MediaID like '1___';
```

### 2. What are the names of all the movies that have been purchased?

```sql
select Title from MyMedia
where MediaID like '9___';
```

### 3. Count the number of Pop songs that have been purchased
```sql
select COUNT(*)
from MyMusic
where Genre = 'Pop';
```

### 4. Return the titles of the films purchased from Amazon
```sql
select Title
from MyMedia
where MediaID IN (
	select PurchaseInfo.mediaID
	from PurchaseInfo
	where PurchaseInfo.Shop = 'Amazon');
```

### 5. Return the average amount spent on movies/music purchased
```sql
select AVG(Price)
from PurchaseInfo;
```

### 6. Return the average price of Ariana Grande's songs
```sql
select AVG(Price)
from PurchaseInfo
where MediaID IN (
	select mediaID
    from MyMusic
    where Artist = 'Ariana Grande');
```
### 7. Return all of the films starring Leonardo DiCaprio
```sql
select Title
from MyMovies
where KeyActors like '%Leonardo DiCaprio%';
```

### 8. Return all the movies released in the 20th century

```sql
select Title
from MyMovies
where ReleaseYear like '19__';
```

### 9. Return the URL from where the movie "Toy Story 4" was purchased
```sql
select URL
from PurchaseInfo
where MediaID IN (
	select MediaID
    from MyMedia
    where Title = 'Toy Story 4');
```

### 10. Return the name of the shop with most purchases

```sql
select Shop, COUNT(Shop) AS shopCount
from PurchaseInfo
GROUP BY Shop
ORDER BY COUNT(Shop) DESC
LIMIT 1;
```

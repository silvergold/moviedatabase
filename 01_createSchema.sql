
DROP DATABASE IF EXISTS Media;

CREATE DATABASE Media;

USE Media;

DROP TABLE IF EXISTS MyMedia;
DROP TABLE IF EXISTS MyMovies;
DROP TABLE IF EXISTS MyMusic;
DROP TABLE IF EXISTS PurchaseInfo;


CREATE TABLE MyMedia
(MediaID INT NOT NULL,
Title VARCHAR(50) NOT NULL,
ReleaseYear CHAR(4) NOT NULL,
PRIMARY KEY(MediaID));

CREATE TABLE MyMovies
(MediaID INT NOT NULL,
Title VARCHAR(50) NOT NULL,
ReleaseYear CHAR(4) NOT NULL,
Genre VARCHAR(50),
Synopsis VARCHAR (500),
KeyActors VARCHAR(100),
PRIMARY KEY(Title,ReleaseYear),
FOREIGN KEY(MediaID) REFERENCES MyMedia(MediaID));

CREATE TABLE MyMusic
(MediaID INT NOT NULL,
Title VARCHAR(50) NOT NULL,
ReleaseYear CHAR(4) NOT NULL,
Album VARCHAR(50) NOT NULL,
Genre VARCHAR(10),
Artist VARCHAR(50),
PRIMARY KEY(Title,Album),
FOREIGN KEY(MediaID) REFERENCES MyMedia(MediaID));

CREATE TABLE PurchaseInfo
(OrderID INT NOT NULL,
MediaID INT NOT NULL,
Shop VARCHAR(20),
DateofPurchase DATETIME,
Price DECIMAL(10,2),
LocationOnline CHAR(1),
URL VARCHAR(100),
PRIMARY KEY(OrderID),
FOREIGN KEY(MediaID) REFERENCES MyMedia(MediaID));

INSERT INTO MyMedia (MediaID,Title,ReleaseYear) VALUES(1001,'7 rings','2018');
INSERT INTO MyMedia (MediaID,Title,ReleaseYear) VALUES(1002,'thank u, next','2018');
INSERT INTO MyMedia (MediaID,Title,ReleaseYear) VALUES(1003,'One Last Time','2014');
INSERT INTO MyMedia (MediaID,Title,ReleaseYear) VALUES(1004,'Everyday Is Christmas','2017');
INSERT INTO MyMedia (MediaID,Title,ReleaseYear) VALUES(1005,'Chandelier','2014');
INSERT INTO MyMedia (MediaID,Title,ReleaseYear) VALUES(1006,'Cheap Thrills','2016');
INSERT INTO MyMedia (MediaID,Title,ReleaseYear) VALUES(9007,'Gone Girl','2014');
INSERT INTO MyMedia (MediaID,Title,ReleaseYear) VALUES(9008,'Wolf of Wall Street','2013');
INSERT INTO MyMedia (MediaID,Title,ReleaseYear) VALUES(9009,'The Girl on the Train','2015');
INSERT INTO MyMedia (MediaID,Title,ReleaseYear) VALUES(9010,'The Legend of 1900','1998');
INSERT INTO MyMedia (MediaID,Title,ReleaseYear) VALUES(9011,'Toy Story 4','2019');
INSERT INTO MyMedia (MediaID,Title,ReleaseYear) VALUES(9012,'Titanic','1997');
INSERT INTO MyMedia (MediaID,Title,ReleaseYear) VALUES(1007,'The London','2019');

INSERT INTO MyMovies (MediaID, Title, ReleaseYear, Genre, Synopsis, KeyActors) VALUES(9007,'Gone Girl', '2014', 'Thriller', 'With his wife`s disappearance having become the focus of an intense media circus, a man sees the spotlight turned on him when it`s suspected that he may not be innocent',
'Ben Affleck, Rosamund Pike, Neil Patrick Harris, Tyler Perry');

INSERT INTO MyMovies (MediaID, Title, ReleaseYear, Genre, Synopsis, KeyActors) VALUES(9008, 'Wolf of Wall Street', '2013', 'Crime',
'In 1987, Jordan Belfort (Leonardo DiCaprio) takes an entry-level job at a Wall Street brokerage firm. By the early 1990s, while still in his 20s, Belfort founds his own firm, Stratton Oakmont. Together with his trusted lieutenant (Jonah Hill) and a merry band of brokers, Belfort makes a huge fortune by defrauding wealthy investors out of millions. However, while Belfort and his cronies partake in a hedonistic brew of sex, drugs and thrills, the SEC and the FBI close in on his empire of excess',
'Leonardo DiCaprio, Jonah Hill, Margot Robbie');

INSERT INTO MyMovies (MediaID, Title, ReleaseYear, Genre, Synopsis, KeyActors) VALUES(9009, 'The Girl on the Train', '2016', 'Mystery',
'Commuter Rachel Watson (Emily Blunt) catches daily glimpses of a seemingly perfect couple, Scott and Megan, from the window of her train. One day, Watson witnesses something shocking unfold in the backyard of the strangers home. Rachel tells the authorities what she thinks she saw after learning that Megan is now missing and feared dead. Unable to trust her own memory, the troubled woman begins her own investigation, while police suspect that Rachel may have crossed a dangerous line',
'Emily Blunt, Rebecca Ferguson');

INSERT INTO MyMovies (MediaID, Title, ReleaseYear, Genre, Synopsis, KeyActors) VALUES(9010, 'The Legend of 1900', '1998', 'International', 'On New Years Day 1900, Danny, a crew member on an ocean liner, finds a deserted infant and decides to adopt him',
'Tim Roth, Pruitt Taylor Vince. Mélanie Thierry');

INSERT INTO MyMovies (MediaID, Title, ReleaseYear, Genre, Synopsis, KeyActors) VALUES(9011, 'Toy Story 4', '2019', 'Animation', 'Some toys come to life and start talking',
'Tom Hanks, Tim Allen, Joan Cusack');

INSERT INTO MyMovies(MediaID, Title, ReleaseYear, Genre, Synopsis, KeyActors) VALUES(9012, 'Titanic', '1997', 'Romance', 'A ship crashes into an iceberg and people died', 'Leonardo DiCaprio, Kate Winslet');

INSERT INTO MyMusic(MediaID, Title, ReleaseYear, Album, Genre, Artist) VALUES(1001, '7 rings', '2018', 'thank u, next', 'Pop', 'Ariana Grande');
INSERT INTO MyMusic(MediaID, Title, ReleaseYear, Album, Genre, Artist) VALUES (1002, 'thank u, next', '2018', 'thank u, next', 'Pop', 'Ariana Grande');
INSERT INTO MyMusic(MediaID, Title, ReleaseYear, Album, Genre, Artist) VALUES(1003, 'One Last Time', '2018', 'My Everything', 'Pop', 'Ariana Grande');
INSERT INTO MyMusic(MediaID, Title, ReleaseYear, Album, Genre, Artist) VALUES(1004, 'Everyday is Christmas', '2017', 'Everyday is Christmas', 'Pop', 'Sia');
INSERT INTO MyMusic(MediaID, Title, ReleaseYear, Album, Genre, Artist) VALUES(1005, 'Chandelier', '2014', '1000 Forms of Fear', 'Pop', 'Sia');
INSERT INTO MyMusic(MediaID, Title, ReleaseYear, Album, Genre, Artist) VALUES(1006, 'Cheap Thrills', '2016', 'This is Acting', 'Pop', 'Sia');
INSERT INTO MyMusic(MediaID, Title, ReleaseYear, Album, Genre, Artist) VALUES(1007, 'The London', '2019', 'So Much Fun', 'Rap', 'Young Thug');


INSERT INTO PurchaseInfo(OrderID, MediaID, Shop, DateofPurchase, Price, LocationOnline, URL) VALUES(80228346, 1001, 'Apple Music', '2018-10-31 21:30:32', '0.99', 1, 'https://music.apple.com/us/album/7-rings-remix-feat-2-chainz-single/1451153783' );

INSERT INTO PurchaseInfo(OrderID, MediaID, Shop, DateofPurchase, Price, LocationOnline, URL) VALUES(80227358, 1002, 'Apple Music', '2018-07-25 17:00:34', '1.49', 1, 'https://music.apple.com/gb/album/thank-u-next/1450330588' );

INSERT INTO PurchaseInfo(OrderID, MediaID, Shop, DateofPurchase, Price, LocationOnline, URL) VALUES(10100935, 1003, 'HMV', '2014-11-10 12:40:52', '1.89', 0, NULL );

INSERT INTO PurchaseInfo(OrderID, MediaID, Shop, DateofPurchase, Price, LocationOnline, URL) VALUES(10203400, 1004, 'HMV', '2017-12-30 09:23:00', '2.39', 0, NULL );

INSERT INTO PurchaseInfo(OrderID, MediaID, Shop, DateofPurchase, Price, LocationOnline, URL) VALUES(80098760, 1005, 'Apple Music', '2014-06-21 23:45:23', '0.99', 1, 'https://music.apple.com/th/album/chandelier-single/838293746' );

INSERT INTO PurchaseInfo(OrderID, MediaID, Shop, DateofPurchase, Price, LocationOnline, URL) VALUES(10168760, 1006, 'HMV', '2016-08-15 20:32:16', '2.19', 0, NULL);

INSERT INTO PurchaseInfo(OrderID, MediaID, Shop, DateofPurchase, Price, LocationOnline, URL) VALUES(10098926, 9007, 'HMV', '2014-10-09 22:04:37', '5.99', 0, NULL);

INSERT INTO PurchaseInfo(OrderID, MediaID, Shop, DateofPurchase, Price, LocationOnline, URL) VALUES(10042548, 9008, 'HMV', '2013-01-15 13:32:53', '5.99', 0, NULL);

INSERT INTO PurchaseInfo(OrderID, MediaID, Shop, DateofPurchase, Price, LocationOnline, URL) VALUES(10137788, 9009, 'HMV', '2015-09-30 21:24:16', '5.99', 0, NULL);

INSERT INTO PurchaseInfo(OrderID, MediaID, Shop, DateofPurchase, Price, LocationOnline, URL) VALUES(40168761, 9010, 'Amazon', '2013-08-18 23:16:51', '7.99', 1, 'https://www.amazon.com/Legend-1900-Tim-Roth/dp/B07939LVYM' );

INSERT INTO PurchaseInfo(OrderID, MediaID, Shop, DateofPurchase, Price, LocationOnline, URL) VALUES(40168762, 9011, 'Amazon', '2019-08-15 20:32:16', '13.99', 1, 'https://www.amazon.co.uk/Toy-Story-Tom-Hanks-Woody/dp/B07THKVTQM' );

INSERT INTO PurchaseInfo(OrderID, MediaID, Shop, DateofPurchase, Price, LocationOnline, URL) VALUES(40168763, 9012, 'Amazon', '2016-07-03 22:51:34', '7.99', 1, 'https://www.amazon.co.uk/Titanic-Leonardo-DiCaprio/dp/B019HYEMA8' );

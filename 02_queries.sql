-- 1.Query for the purchase info of all the music

select * from PurchaseInfo
where MediaID like '1___';

-- 2.Query for the titles of all the movies purchased

select Title from MyMedia
where MediaID like '9___';

-- 3.Count the number of pop songs purchased

select COUNT(*)
from MyMusic
where Genre = 'Pop';

-- 4.Return the titles of the films purchased from Amazon (join)

select Title
from MyMedia
where MediaID IN (
	select PurchaseInfo.mediaID
	from PurchaseInfo
	where PurchaseInfo.Shop = 'Amazon');

-- 5.Return the average amount spent on movies/music purchased online

select AVG(Price)
from PurchaseInfo;

-- 6.Return the average price of Ariana Grande's songs (join)

select AVG(Price)
from PurchaseInfo
where MediaID IN (
	select mediaID
    from MyMusic
    where Artist = 'Ariana Grande');

-- 7.Return all the films starred by Leonardo Dicaprio

select Title
from MyMovies
where KeyActors like '%Leonardo DiCaprio%';

-- 8.Return all the movies from the 20s century (wildcard)

select Title
from MyMovies
where ReleaseYear like '19__';

-- 9.Return the URL where the movie "Toy Story 4" is purchased

select URL
from PurchaseInfo
where MediaID IN (
	select MediaID
    from MyMedia
    where Title = 'Toy Story 4');

-- 10.Return the name of the shop with most purchases

select Shop, COUNT(Shop) AS shopCount
from PurchaseInfo
GROUP BY Shop
ORDER BY COUNT(Shop) DESC
LIMIT 1;